package ua.edu.ukma.fcs.sweetshop.domain.invoices;

public interface IInvoicePaymentObservable {
    void subscribe(final IInvoicePaymentObserver observer);
    void unsubscribe(final IInvoicePaymentObserver observer);
}
