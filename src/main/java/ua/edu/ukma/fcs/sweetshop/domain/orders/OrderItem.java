package ua.edu.ukma.fcs.sweetshop.domain.orders;

import java.util.Objects;
import org.apache.commons.lang3.Validate;
import ua.edu.ukma.fcs.sweetshop.common.data.MoneySum;
import ua.edu.ukma.fcs.sweetshop.domain.EntityId;

public class OrderItem {
    private final EntityId id;

    private final String name;
    private final MoneySum price;

    public OrderItem(final EntityId id, final String name, final MoneySum price) {
        Validate.notNull(id);
        Validate.notNull(price);

        this.id = id;
        this.name = name;
        this.price = price;
    }

    public EntityId getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public MoneySum getPrice() {
        return price;
    }

    @Override
    public int hashCode() {
        return this.id.getValue().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OrderItem other = (OrderItem) obj;
        return Objects.equals(this.id, other.id);
    }
}
