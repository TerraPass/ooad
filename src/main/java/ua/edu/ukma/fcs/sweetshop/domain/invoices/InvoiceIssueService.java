package ua.edu.ukma.fcs.sweetshop.domain.invoices;

import ua.edu.ukma.fcs.sweetshop.domain.IRepository;
import ua.edu.ukma.fcs.sweetshop.domain.orders.Order;

public class InvoiceIssueService implements IInvoiceIssueService {
    private final IInvoiceFactory invoiceFactory;
    private final IRepository<Invoice> invoiceRepository;

    public InvoiceIssueService(IInvoiceFactory invoiceFactory, IRepository<Invoice> invoiceRepository) {
        this.invoiceFactory = invoiceFactory;
        this.invoiceRepository = invoiceRepository;
    }

    @Override
    public Invoice issueInvoiceForOrder(Order order) {
        final Invoice invoice = invoiceFactory.fromOrder(order);
        invoiceRepository.insert(invoice);
        return invoice;
    }
}
