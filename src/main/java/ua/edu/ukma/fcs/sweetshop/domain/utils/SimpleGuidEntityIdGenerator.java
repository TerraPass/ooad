package ua.edu.ukma.fcs.sweetshop.domain.utils;

import ua.edu.ukma.fcs.sweetshop.domain.EntityId;

public class SimpleGuidEntityIdGenerator implements IEntityIdGenerator {

    @Override
    public EntityId generate() {
        return new EntityId(java.util.UUID.randomUUID().toString());
    }
}
