package ua.edu.ukma.fcs.sweetshop.integration.payments.impl;

import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.Validate;
import ua.edu.ukma.fcs.sweetshop.integration.payments.data.ChargeError;
import ua.edu.ukma.fcs.sweetshop.integration.payments.data.ChargeRequest;
import ua.edu.ukma.fcs.sweetshop.integration.payments.data.ChargeResult;

public class StripeSDKCardChargerImpl implements ICardChargerImpl {
    private final String stripeApiKey;

    public StripeSDKCardChargerImpl(final String stripeApiKey) {
        Validate.notBlank(stripeApiKey, "stripeApiKey must not be null or empty string");

        this.stripeApiKey = stripeApiKey;
    }

    @Override
    public ChargeResult performCharge(final ChargeRequest chargeRequest) {
        Map<String, Object> chargeParams = new HashMap<>();
        chargeParams.put("amount", chargeRequest.getMoneySum().getAmount());
        chargeParams.put("currency", chargeRequest.getMoneySum().getCurrency().getIsoCode());
        chargeParams.put("source", chargeRequest.getCard().getStripeToken());
        chargeParams.put("description", chargeRequest.getDescription());

        try {
            final com.stripe.model.Charge stripeCharge = com.stripe.model.Charge.create(chargeParams, this.stripeApiKey);
            
            return ChargeResult.fromSuccess(chargeRequest, stripeCharge.getId());
        } catch (AuthenticationException e) {
            return ChargeResult.fromError(chargeRequest, ChargeError.AUTHENTICATION, e.getMessage());
        }  catch (APIConnectionException e) {
            return ChargeResult.fromError(chargeRequest, ChargeError.API_CONNECTION, e.getMessage());
        } catch (CardException e) {
            return ChargeResult.fromError(chargeRequest, ChargeError.CARD, e.getMessage());
        } catch (APIException|InvalidRequestException e) {
            return ChargeResult.fromError(chargeRequest, ChargeError.UNKNOWN, e.getMessage());
        }
    }
}
