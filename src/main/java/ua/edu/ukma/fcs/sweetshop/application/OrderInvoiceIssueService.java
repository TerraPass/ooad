package ua.edu.ukma.fcs.sweetshop.application;

import ua.edu.ukma.fcs.sweetshop.domain.IRepository;
import ua.edu.ukma.fcs.sweetshop.application.dto.IOrderInvoiceIssueResultFactory;
import ua.edu.ukma.fcs.sweetshop.application.dto.OrderInvoiceIssueResult;
import ua.edu.ukma.fcs.sweetshop.application.dto.OrderInvoiceRequest;
import ua.edu.ukma.fcs.sweetshop.domain.orders.Order;
import ua.edu.ukma.fcs.sweetshop.domain.orders.OrderInvoice;

public class OrderInvoiceIssueService implements IOrderInvoiceIssueService {
    private final IRepository<Order> orderRepository;
    
    private final IOrderInvoiceIssueResultFactory orderInvoiceIssueResultFactory;

    public OrderInvoiceIssueService(IRepository<Order> orderRepository, IOrderInvoiceIssueResultFactory orderInvoiceIssueResultFactory) {
        this.orderRepository = orderRepository;
        this.orderInvoiceIssueResultFactory = orderInvoiceIssueResultFactory;
    }
    
    @Override
    public OrderInvoiceIssueResult issueOrderInvoice(final OrderInvoiceRequest request) {
        final Order order  = this.orderRepository.getById(request.getOrderId());
        if(order == null) {
            return orderInvoiceIssueResultFactory.fromNoSuchOrder();
        }

        final OrderInvoice issuedOrderInvoice = order.issueInvoice();

        return this.orderInvoiceIssueResultFactory.fromNewOrderInvoice(issuedOrderInvoice);
    }
    
}
