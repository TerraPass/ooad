package ua.edu.ukma.fcs.sweetshop.domain.invoices;

import ua.edu.ukma.fcs.sweetshop.domain.EntityId;

public interface IInvoicePaymentObserver {
    void onInvoicePaid(final EntityId invoiceId);
}
