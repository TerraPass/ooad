package ua.edu.ukma.fcs.sweetshop.application.dto;

import ua.edu.ukma.fcs.sweetshop.web.forms.InvoiceCardPaymentRawFormData;

public interface IInvoiceCardPaymentRequestFactory {
    InvoiceCardPaymentRequest fromRawFormData(final InvoiceCardPaymentRawFormData rawFormData)
        throws InvalidFormDataException;
}
