package ua.edu.ukma.fcs.sweetshop.integration.payments.impl;

import ua.edu.ukma.fcs.sweetshop.integration.payments.data.ChargeRequest;
import ua.edu.ukma.fcs.sweetshop.integration.payments.data.ChargeResult;

public interface ICardChargerImpl {
    ChargeResult performCharge(final ChargeRequest chargeRequest);
}
