package ua.edu.ukma.fcs.sweetshop.domain.orders;

import java.util.List;
import ua.edu.ukma.fcs.sweetshop.domain.EntityId;

public interface IOrderReadRepository {
    Order getById();
    List<Order> getAll();

    Order getByInvoiceId(final EntityId invoiceId);
}
