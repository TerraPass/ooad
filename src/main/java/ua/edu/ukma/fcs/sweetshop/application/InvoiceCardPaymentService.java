package ua.edu.ukma.fcs.sweetshop.application;

import java.util.Set;
import ua.edu.ukma.fcs.sweetshop.domain.IRepository;
import ua.edu.ukma.fcs.sweetshop.application.dto.InvoiceCardPaymentRequest;
import ua.edu.ukma.fcs.sweetshop.application.dto.InvoiceCardPaymentResult;
import ua.edu.ukma.fcs.sweetshop.domain.EntityId;
import ua.edu.ukma.fcs.sweetshop.domain.invoices.IInvoicePaymentObservable;
import ua.edu.ukma.fcs.sweetshop.domain.invoices.Invoice;
import ua.edu.ukma.fcs.sweetshop.domain.invoices.IInvoicePaymentObserver;

public class InvoiceCardPaymentService implements IInvoiceCardPaymentService, IInvoicePaymentObservable {
    private IRepository<Invoice> invoiceRepository;

    private Set<IInvoicePaymentObserver> invoicePaymentObservers;

    @Override
    public InvoiceCardPaymentResult payInvoiceByCard(InvoiceCardPaymentRequest invoiceCardPaymentRequest) {
        final EntityId invoiceId = invoiceCardPaymentRequest.getInvoiceId();
        final Invoice invoice = invoiceRepository.getById(invoiceId);
        
        final InvoiceCardPaymentResult result = invoice.makeCardPayment(invoiceCardPaymentRequest.getCard());
        
        invoiceRepository.update(invoice);
        
        this.notifyObservers(invoiceId);
        
        return result;
    }
    
    @Override
    public void subscribe(IInvoicePaymentObserver observer) {
        this.invoicePaymentObservers.add(observer);
    }

    @Override
    public void unsubscribe(IInvoicePaymentObserver observer) {
        this.invoicePaymentObservers.remove(observer);
    }

    private void notifyObservers(final EntityId invoiceId) {
        // FIXME: Use while and manually shift iterator 
        // to allow safe unsubscription of observers.
        for(final IInvoicePaymentObserver observer : invoicePaymentObservers) {
            observer.onInvoicePaid(invoiceId);
        }
    }
}
