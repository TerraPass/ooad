package ua.edu.ukma.fcs.sweetshop.web.forms;

import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class OrderPlacementRawFormData implements Serializable {
    @XmlElement
    private String customerId;
    @XmlElement
    private List<String> orderItemsIds;
    @XmlElement
    private List<Integer> orderItemsQuantities;
    @XmlElement
    private String deliveryAddress;

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public List<String> getOrderItemsIds() {
        return orderItemsIds;
    }

    public void setOrderItemsIds(List<String> orderItemsIds) {
        this.orderItemsIds = orderItemsIds;
    }

    public List<Integer> getOrderItemsQuantities() {
        return orderItemsQuantities;
    }

    public void setOrderItemsQuantities(List<Integer> orderItemsQuantities) {
        this.orderItemsQuantities = orderItemsQuantities;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }
}
