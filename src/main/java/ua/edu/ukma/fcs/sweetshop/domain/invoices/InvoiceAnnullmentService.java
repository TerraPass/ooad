package ua.edu.ukma.fcs.sweetshop.domain.invoices;

import ua.edu.ukma.fcs.sweetshop.domain.EntityId;
import ua.edu.ukma.fcs.sweetshop.domain.IRepository;

public class InvoiceAnnullmentService implements IInvoiceAnnullmentService {
    private final IRepository<Invoice> invoiceRepository;

    public InvoiceAnnullmentService(IRepository<Invoice> invoiceRepository) {
        this.invoiceRepository = invoiceRepository;
    }

    @Override
    public void annullInvoiceById(final EntityId invoiceId) {
        final Invoice invoice = this.invoiceRepository.getById(invoiceId);
        this.invoiceRepository.update(invoice);
    }
}
