package ua.edu.ukma.fcs.sweetshop.domain.orders;

import ua.edu.ukma.fcs.sweetshop.domain.invoices.Invoice;

public interface IOrderInvoiceFactory {
    OrderInvoice fromInvoice(final Invoice invoice);
}
