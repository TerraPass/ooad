package ua.edu.ukma.fcs.sweetshop.integration.payments.data;

public enum ChargeError {
    NONE,
    UNKNOWN,
    AUTHENTICATION,
    API_CONNECTION,
    CARD
}
