package ua.edu.ukma.fcs.sweetshop.application.dto;

import org.apache.commons.lang3.Validate;
import ua.edu.ukma.fcs.sweetshop.common.data.Card;
import ua.edu.ukma.fcs.sweetshop.domain.EntityId;

public final class InvoiceCardPaymentRequest {
    private final EntityId invoiceId;
    private final Card card;

    public InvoiceCardPaymentRequest(final String invoiceId, final Card card) {
        Validate.notBlank(invoiceId, "invoiceId must not be null or empty");
        Validate.notNull(card, "card must not be null");

        this.invoiceId = new EntityId(invoiceId);
        this.card = card;
    }

    public EntityId getInvoiceId() {
        return invoiceId;
    }

    public Card getCard() {
        return card;
    }
}
