package ua.edu.ukma.fcs.sweetshop.web.forms;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class InvoiceCardPaymentRawFormData implements Serializable {
    private String invoiceId;
    @XmlElement
    private String cardStripeToken;

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }
    
    public String getCardStripeToken() {
        return cardStripeToken;
    }

    public void setCardStripeToken(String cardStripeToken) {
        this.cardStripeToken = cardStripeToken;
    }
}
