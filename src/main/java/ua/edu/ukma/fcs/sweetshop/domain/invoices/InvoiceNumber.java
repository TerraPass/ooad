package ua.edu.ukma.fcs.sweetshop.domain.invoices;

import org.apache.commons.lang3.Validate;

public final class InvoiceNumber {
    private final String value;

    public InvoiceNumber(final String value) {
        Validate.notBlank(value, "value must not be null or empty");

        this.value = value;
    }

    @Override
    public String toString() {
        return this.value;
    }
}
