package ua.edu.ukma.fcs.sweetshop.common.utils;

import java.util.List;
//import ua.edu.ukma.fcs.sweetshop.common.data.Currency;
import ua.edu.ukma.fcs.sweetshop.common.data.MoneySum;

public interface IMoneySumCalculator {
    MoneySum multiply(final Integer multiplier, final MoneySum moneySum);
    MoneySum add(final MoneySum moneySumA, final MoneySum moneySumB);

    MoneySum sum(final List<MoneySum> moneySums/*, final Currency currency*/);
}
