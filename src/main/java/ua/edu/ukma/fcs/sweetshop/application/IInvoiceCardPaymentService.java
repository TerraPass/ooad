package ua.edu.ukma.fcs.sweetshop.application;

import ua.edu.ukma.fcs.sweetshop.application.dto.InvoiceCardPaymentRequest;
import ua.edu.ukma.fcs.sweetshop.application.dto.InvoiceCardPaymentResult;

public interface IInvoiceCardPaymentService {
    InvoiceCardPaymentResult payInvoiceByCard(final InvoiceCardPaymentRequest invoiceCardPaymentRequest);
}
