package ua.edu.ukma.fcs.sweetshop.application.dto;

import org.apache.commons.lang3.Validate;
import ua.edu.ukma.fcs.sweetshop.domain.EntityId;

public final class OrderInvoiceRequest {
    private final EntityId orderId;
    private final String billingAddress;

    public OrderInvoiceRequest(final EntityId orderId, final String billingAddress) {
        Validate.notNull(orderId);
        Validate.notBlank(billingAddress);

        this.orderId = orderId;
        this.billingAddress = billingAddress;
    }

    public EntityId getOrderId() {
        return orderId;
    }

    public String getBillingAddress() {
        return billingAddress;
    }
}
