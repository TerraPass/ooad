package ua.edu.ukma.fcs.sweetshop.application.dto;

import ua.edu.ukma.fcs.sweetshop.web.forms.OrderPlacementRawFormData;

public interface IOrderFormDataFactory {
    OrderFormData fromRawFormData(final OrderPlacementRawFormData rawFormData)
        throws InvalidFormDataException;
}
