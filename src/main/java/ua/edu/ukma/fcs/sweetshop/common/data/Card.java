package ua.edu.ukma.fcs.sweetshop.common.data;

import org.apache.commons.lang3.Validate;

public final class Card {
    private final String stripeToken;

    public Card(final String stripeToken) {
        Validate.notBlank(stripeToken, "stripeToken must not be null or empty string");

        this.stripeToken = stripeToken;
    }

    public String getStripeToken() {
        return stripeToken;
    }
}
