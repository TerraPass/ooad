package ua.edu.ukma.fcs.sweetshop.domain.invoices;

import ua.edu.ukma.fcs.sweetshop.integration.payments.data.ChargeResult;

public interface IChargeOutcomeFactory {
    ChargeOutcome fromChargeResult(final ChargeResult chargeResult);
}
