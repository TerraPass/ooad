package ua.edu.ukma.fcs.sweetshop.integration.payments;

import org.apache.commons.lang3.Validate;
import ua.edu.ukma.fcs.sweetshop.integration.payments.data.ChargeRequest;
import ua.edu.ukma.fcs.sweetshop.integration.payments.data.ChargeResult;
import ua.edu.ukma.fcs.sweetshop.integration.payments.impl.ICardChargerImpl;

public class CardCharger implements ICardCharger {
    private final ICardChargerImpl impl;

    // TODO: Should we really accept impl from outside?
    public CardCharger(final ICardChargerImpl impl) {
        Validate.notNull(impl, "impl must not be null");

        this.impl = impl;
    }

    @Override
    public ChargeResult chargeCard(final ChargeRequest chargeRequest) {
        return this.impl.performCharge(chargeRequest);
    }
}
