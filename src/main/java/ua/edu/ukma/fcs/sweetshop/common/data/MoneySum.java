package ua.edu.ukma.fcs.sweetshop.common.data;

import org.apache.commons.lang3.Validate;

public final class MoneySum {
    public static final MoneySum ZERO = new MoneySum(0, Currency.USD);

    private final long amount;
    private final Currency currency;

    public MoneySum(final long amount, final Currency currency) {
        Validate.notNull(currency, "currency must not be null");

        this.amount = amount;
        this.currency = currency;
    }

    public long getAmount() {
        return amount;
    }

    public Currency getCurrency() {
        return currency;
    }
}
