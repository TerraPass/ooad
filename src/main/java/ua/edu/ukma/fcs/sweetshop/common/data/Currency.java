package ua.edu.ukma.fcs.sweetshop.common.data;

import java.util.Objects;
import org.apache.commons.lang3.Validate;

public final class Currency {
    public static final Currency USD = new Currency("USD");

    private final java.util.Currency currency;

    public Currency(final String isoCode) {
        Validate.notBlank(isoCode, "isoCode must not be null or empty string");

        this.currency = java.util.Currency.getInstance(isoCode);
    }

    public String getIsoCode() {
        return currency.getCurrencyCode();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.getIsoCode());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Currency other = (Currency) obj;
        return Objects.equals(this.getIsoCode(), other.getIsoCode());
    }

    
}
