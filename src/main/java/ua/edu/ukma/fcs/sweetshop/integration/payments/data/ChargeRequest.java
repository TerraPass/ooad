package ua.edu.ukma.fcs.sweetshop.integration.payments.data;

import org.apache.commons.lang3.Validate;
import ua.edu.ukma.fcs.sweetshop.common.data.Card;
import ua.edu.ukma.fcs.sweetshop.common.data.MoneySum;

public final class ChargeRequest {
    private final Card card;
    private final MoneySum moneySum;
    private final String description;

    public ChargeRequest(final Card card, final MoneySum moneySum, final String description) {
        Validate.notNull(card, "card must not be null");
        Validate.notNull(moneySum, "moneySum must not be null");
        Validate.notNull(description, "description must not be null");

        this.card = card;
        this.moneySum = moneySum;
        this.description = description;
    }

    public Card getCard() {
        return card;
    }

    public MoneySum getMoneySum() {
        return moneySum;
    }

    public String getDescription() {
        return description;
    }
}
