package ua.edu.ukma.fcs.sweetshop.web.controllers;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import ua.edu.ukma.fcs.sweetshop.application.IInvoiceCardPaymentService;
import ua.edu.ukma.fcs.sweetshop.application.dto.IInvoiceCardPaymentRequestFactory;
import ua.edu.ukma.fcs.sweetshop.application.dto.InvalidFormDataException;
import ua.edu.ukma.fcs.sweetshop.application.dto.InvoiceCardPaymentRequest;
import ua.edu.ukma.fcs.sweetshop.application.dto.InvoiceCardPaymentResult;
import ua.edu.ukma.fcs.sweetshop.web.forms.InvoiceCardPaymentRawFormData;
import ua.edu.ukma.fcs.sweetshop.web.views.IInvoiceView;

@Path("invoices")
public class InvoiceController {
    private IInvoiceView invoiceView;

    private IInvoiceCardPaymentRequestFactory invoiceCardPaymentRequestFactory;
    private IInvoiceCardPaymentService invoiceCardPaymentService;

    @POST
    @Path("{id}/payments")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response payByCard(
        @PathParam("id") final String invoiceId, 
        final InvoiceCardPaymentRawFormData rawFormData)
    {
        rawFormData.setInvoiceId(invoiceId);

        try {
            final InvoiceCardPaymentRequest invoiceCardPaymentRequest
                = this.invoiceCardPaymentRequestFactory.fromRawFormData(rawFormData);

            final InvoiceCardPaymentResult result 
                = this.invoiceCardPaymentService.payInvoiceByCard(invoiceCardPaymentRequest);

            return this.invoiceView.getCardPaymentResultPresenter().represent(result);
        } catch (InvalidFormDataException e) {
            return this.invoiceView.getErrorPresenter().representValidationFailure(e);
        }
    }
}
