package ua.edu.ukma.fcs.sweetshop.domain.invoices;

import java.util.Optional;
import org.apache.commons.lang3.Validate;
import ua.edu.ukma.fcs.sweetshop.common.data.MoneySum;
import ua.edu.ukma.fcs.sweetshop.domain.EntityId;

public final class Payment {
    private final EntityId id;

    public static enum Media {
        CARD,
        CASH,
        CHECK,
        WIRE
    }
    
    public static enum Status {
        NOT_ATTEMPTED,
        FAILED,
        SUCCESSFUL,
        REFUNDED
    }

    private final Media media;
    private final MoneySum moneySum;
    private Status status;
    // TODO
    //private final LocalDate date;

    private Optional<Charge> cardCharge;

    public Payment(final EntityId id, final Media media, final MoneySum moneySum) {
        Validate.notNull(id, "id must not be null");
        Validate.notNull(media, "media must not be null");
        Validate.notNull(moneySum, "moneySum must not be null");

        this.id = id;
        this.media = media;
        this.moneySum = moneySum;
        this.cardCharge = Optional.empty();

        this.status = Status.NOT_ATTEMPTED;
    }
    
    public void paidByCardCharge(final Charge charge) {
        if(this.media != Media.CARD) {
            throw new IllegalStateException(
                "This operation is only valid for CARD payments"
            );
        }

        this.cardCharge = Optional.of(charge);
        this.status = Status.SUCCESSFUL;
    }

    public void fail() {
        this.status = Status.FAILED;
    }
    
    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
