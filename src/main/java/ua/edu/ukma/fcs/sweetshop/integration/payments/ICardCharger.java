package ua.edu.ukma.fcs.sweetshop.integration.payments;

import ua.edu.ukma.fcs.sweetshop.integration.payments.data.ChargeRequest;
import ua.edu.ukma.fcs.sweetshop.integration.payments.data.ChargeResult;

public interface ICardCharger {
    ChargeResult chargeCard(final ChargeRequest chargeRequest);
}
