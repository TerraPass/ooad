package ua.edu.ukma.fcs.sweetshop.domain.invoices;

import ua.edu.ukma.fcs.sweetshop.domain.orders.Order;

public interface IInvoiceIssueService {
    Invoice issueInvoiceForOrder(final Order order);
}
