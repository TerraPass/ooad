package ua.edu.ukma.fcs.sweetshop.web.views;

import javax.ws.rs.core.Response;
import ua.edu.ukma.fcs.sweetshop.application.dto.InvalidFormDataException;

public interface IErrorPresenter {
    Response representValidationFailure(final InvalidFormDataException e);
}
