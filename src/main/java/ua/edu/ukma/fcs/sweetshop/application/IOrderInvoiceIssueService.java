package ua.edu.ukma.fcs.sweetshop.application;

import ua.edu.ukma.fcs.sweetshop.application.dto.OrderInvoiceIssueResult;
import ua.edu.ukma.fcs.sweetshop.application.dto.OrderInvoiceRequest;

public interface IOrderInvoiceIssueService {
    OrderInvoiceIssueResult issueOrderInvoice(final OrderInvoiceRequest request);
}
