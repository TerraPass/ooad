package ua.edu.ukma.fcs.sweetshop.application.dto;

import ua.edu.ukma.fcs.sweetshop.domain.orders.Order;

public interface IOrderPlacementResultFactory {
    OrderPlacementResult fromNewOrder(final Order order);
}
