package ua.edu.ukma.fcs.sweetshop.application.dto;

import ua.edu.ukma.fcs.sweetshop.web.forms.OrderInvoiceRequestRawFormData;

public interface IOrderInvoiceRequestFactory {
    OrderInvoiceRequest fromRawFormData(final OrderInvoiceRequestRawFormData formData) throws InvalidFormDataException;
}
