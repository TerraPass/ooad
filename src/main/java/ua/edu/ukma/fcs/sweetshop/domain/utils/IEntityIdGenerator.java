package ua.edu.ukma.fcs.sweetshop.domain.utils;

import ua.edu.ukma.fcs.sweetshop.domain.EntityId;

public interface IEntityIdGenerator {
    EntityId generate();
}
