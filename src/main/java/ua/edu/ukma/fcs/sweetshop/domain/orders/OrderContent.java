package ua.edu.ukma.fcs.sweetshop.domain.orders;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import ua.edu.ukma.fcs.sweetshop.common.data.MoneySum;
import ua.edu.ukma.fcs.sweetshop.common.utils.IMoneySumCalculator;

public class OrderContent {
    private final Map<OrderItem, Integer> itemQuantities;
    
    public OrderContent() {
        this.itemQuantities = new HashMap<>();
    }

    public Map<OrderItem, Integer> getItemQuantities() {
        return Collections.unmodifiableMap(itemQuantities);
    }

    public void addItem(final OrderItem item, final Integer quantity) {
        if(!itemQuantities.containsKey(item)) {
            itemQuantities.put(item, quantity);
        } else {
            final Integer currentQuantity = itemQuantities.get(item);
            itemQuantities.put(item, currentQuantity + quantity);
        }
    }

    public MoneySum getMoneyTotal(final IMoneySumCalculator calculator) {
        return itemQuantities.entrySet().stream()
            .map(
                (Map.Entry<OrderItem, Integer> entry) 
                    -> calculator.multiply(entry.getValue(), entry.getKey().getPrice())
            )
            .reduce(
                MoneySum.ZERO,
                (a, b) -> calculator.add(a, b)
            );
    }
}
