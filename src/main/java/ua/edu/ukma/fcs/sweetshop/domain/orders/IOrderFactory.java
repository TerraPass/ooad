package ua.edu.ukma.fcs.sweetshop.domain.orders;

import ua.edu.ukma.fcs.sweetshop.domain.orders.Order;
import ua.edu.ukma.fcs.sweetshop.application.dto.OrderFormData;

public interface IOrderFactory {
    Order fromFormData(final OrderFormData formData);
}
