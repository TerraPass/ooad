package ua.edu.ukma.fcs.sweetshop.integration.payments.data;

import ua.edu.ukma.fcs.sweetshop.domain.invoices.Invoice;

public class SimpleChargeDescriber implements IChargeDescriber {
    private static final String DESCRIPTION_TEMPLATE = "Payment for invoice #%d";

    @Override
    public String describeChargeFor(Invoice invoice) {
        return String.format(DESCRIPTION_TEMPLATE, invoice.getNumber());
    }
    
}
