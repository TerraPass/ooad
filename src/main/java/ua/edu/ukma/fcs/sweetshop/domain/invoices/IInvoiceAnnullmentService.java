package ua.edu.ukma.fcs.sweetshop.domain.invoices;

import ua.edu.ukma.fcs.sweetshop.domain.EntityId;

public interface IInvoiceAnnullmentService {
    void annullInvoiceById(final EntityId invoiceId);
}
