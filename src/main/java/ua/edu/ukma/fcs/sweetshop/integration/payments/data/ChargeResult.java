package ua.edu.ukma.fcs.sweetshop.integration.payments.data;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import ua.edu.ukma.fcs.sweetshop.common.data.MoneySum;

public final class ChargeResult {
    private static final String NO_ERROR_MESSAGE = "Charge has been performed successfully";

    private final ChargeRequest request;
    private final String stripeChargeId;

    private final ChargeError error;
    private final String resultMessage;

    private ChargeResult(
        final ChargeRequest request,
        final String stripeChargeId,
        final ChargeError error,
        final String resultMessage
    ) {
        assert request != null : "request must not be null";
        assert (error != null && !ChargeError.NONE.equals(error)) || !StringUtils.isBlank(stripeChargeId)
            : "stripeChargeId must not be blank if there was no error";
        assert error == null || ChargeError.NONE.equals(error) || !StringUtils.isBlank(resultMessage)
            : "resultMessage must not be blank if there was an error";

        this.request = request;
        this.stripeChargeId = stripeChargeId;
        this.error = error == null ? ChargeError.NONE : error;
        this.resultMessage = resultMessage;
    }
    
    public static ChargeResult fromSuccess(final ChargeRequest successfulRequest, final String stripeChargeId) {
        Validate.notNull(successfulRequest, "successfulRequest must not be null");
        Validate.notBlank(stripeChargeId, "stripeChargeId must not be null or empty");

        return new ChargeResult(successfulRequest, stripeChargeId, ChargeError.NONE, NO_ERROR_MESSAGE);
    }
    
    public static ChargeResult fromError(
        final ChargeRequest failedRequest,
        final ChargeError chargeError, 
        final String errorMessage
    ) {
        Validate.notNull(failedRequest, "failedRequest must not be null");
        Validate.notNull(chargeError, "chargeError must not be null");
        Validate.isTrue(!ChargeError.NONE.equals(chargeError), "chargeError must not be NONE in fromError()");

        return new ChargeResult(failedRequest, null, chargeError, errorMessage);
    }
    
    public boolean isSuccessful() {
        return ChargeError.NONE.equals(this.error);
    }
    
    public String getChargeStripeId() {
        if(!this.isSuccessful()) {
            throw new IllegalStateException("Stripe charge id cannot be retrieved from an unsuccessful ChargeResult");
        }

        assert this.stripeChargeId != null : "stripeChargeId must be non-null for a successful ChargeResult";
        
        return this.stripeChargeId;
    }
    
    public ChargeError getError() {
        return error;
    }

    public String getResultMessage() {
        return resultMessage;
    }

    public MoneySum getMoneySum() {
        return request.getMoneySum();
    }

    public String getChargeDescription() {
        return request.getDescription();
    }
}
