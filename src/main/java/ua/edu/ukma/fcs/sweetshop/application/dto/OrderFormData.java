package ua.edu.ukma.fcs.sweetshop.application.dto;

import java.util.Map;
import org.apache.commons.lang3.Validate;

public final class OrderFormData {
    private final String customerId;    // TODO: Should probably contain EntityId instead of plain string.
    private final Map<String, Integer> orderItemQuantities;
    private final String deliveryAddress;

    public OrderFormData(
        final String customerId,
        final Map<String, Integer> orderItemQuantities,
        final String deliveryAddress
    ) {
        Validate.notBlank(customerId, "customerId must not be null or empty");
        Validate.notNull(orderItemQuantities, "orderItemQuantities must not be null");
        Validate.notBlank(deliveryAddress, "deliveryAddress must not be null or empty");

        this.customerId = customerId;
        this.orderItemQuantities = orderItemQuantities;
        this.deliveryAddress = deliveryAddress;
    }

    public String getCustomerId() {
        return customerId;
    }

    public Map<String, Integer> getOrderItemQuantities() {
        return orderItemQuantities;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }
}
