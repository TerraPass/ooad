package ua.edu.ukma.fcs.sweetshop.domain.invoices;

import ua.edu.ukma.fcs.sweetshop.common.data.MoneySum;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.apache.commons.lang3.Validate;
import ua.edu.ukma.fcs.sweetshop.common.data.Card;
import ua.edu.ukma.fcs.sweetshop.application.dto.IInvoiceCardPaymentResultFactory;
import ua.edu.ukma.fcs.sweetshop.application.dto.InvoiceCardPaymentResult;
import ua.edu.ukma.fcs.sweetshop.domain.EntityId;

// So far this is the aggregate root for payments-related entities.
//
// Invoice -1---n- Payment (only 1 successful) -1---1- Charge (optional)
//
public final class Invoice {    
    private final IChargeService chargeService;
    private final IChargeFactory chargeFactory;
    private final IPaymentFactory paymentFactory;
    
    private final IInvoiceCardPaymentResultFactory invoiceCardPaymentResultFactory;
    
    private final EntityId id;

    private final List<Payment> payments;

    private InvoiceNumber number;
    private LocalDate creationDate;
    private Optional<LocalDate> dueDate;
    private MoneySum moneySum;
    private boolean annulled;
    private boolean paid;
    private String billingAddress;

    public Invoice(
        final EntityId id,
        final IChargeService chargeService,
        final IChargeFactory chargeFactory,
        final IPaymentFactory paymentFactory,
        final IInvoiceCardPaymentResultFactory invoiceCardPaymentResultFactory
    ) {
        Validate.notNull(id, "id must not be null");
        Validate.notNull(chargeService, "chargeService must not be null");
        Validate.notNull(chargeFactory, "chargeFactory must not be null");
        Validate.notNull(paymentFactory, "paymentFactory must not be null");
        Validate.notNull(invoiceCardPaymentResultFactory, "invoiceCardPaymentResultFactory must not be null");

        this.chargeService = chargeService;
        this.chargeFactory = chargeFactory;
        this.paymentFactory = paymentFactory;
        this.invoiceCardPaymentResultFactory = invoiceCardPaymentResultFactory;
        
        this.id = id;
        this.paid = false;

        this.payments = new ArrayList<>();
    }

    public InvoiceCardPaymentResult makeCardPayment(final Card card) {
        if(this.paid) {
            return invoiceCardPaymentResultFactory.fromAlreadyPaidInvoice(this);
        }
        if(this.annulled) {
            return invoiceCardPaymentResultFactory.fromAnnulledInvoice(this);
        }

        final Payment cardPayment = paymentFactory.createCardPayment();
        
        final ChargeOutcome chargeOutcome = this.chargeService.chargeByInvoice(this, card);

        if(chargeOutcome.isSuccessful()) {
            final Charge charge = chargeFactory.fromChargeOutcome(chargeOutcome);
            cardPayment.paidByCardCharge(charge);

            this.paid = true;
        } else {
            cardPayment.fail();
        }

        this.payments.add(cardPayment);

        return this.invoiceCardPaymentResultFactory.fromChargeOutcome(this, chargeOutcome);
    }
    
    public void annull() {
        if(this.paid) {
            throw new IllegalStateException("An already paid invoice cannot be annulled");
        }
        this.annulled = true;
    }
    
    public InvoiceNumber getNumber() {
        return number;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public Optional<LocalDate> getDueDate() {
        return dueDate;
    }

    public MoneySum getMoneySum() {
        return this.moneySum;
    }

    void setNumber(final InvoiceNumber number) {
        this.number = number;
    }

    void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    void setDueDate(Optional<LocalDate> dueDate) {
        this.dueDate = dueDate;
    }

    void setMoneySum(MoneySum moneySum) {
        this.moneySum = moneySum;
    }

//    public void setStatus(Status status) {
//        this.status = status;
//    }

    public boolean isPaid() {
        return this.paid;
    }
}
