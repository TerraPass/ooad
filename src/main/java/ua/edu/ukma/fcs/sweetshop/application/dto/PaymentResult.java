package ua.edu.ukma.fcs.sweetshop.application.dto;

public enum PaymentResult {
    SUCCESS,
    ALREADY_PAID,
    FAILURE_INVALID_REQUEST,
    FAILURE_TRY_AGAIN
}
