package ua.edu.ukma.fcs.sweetshop.domain.orders;

import ua.edu.ukma.fcs.sweetshop.domain.EntityId;
import ua.edu.ukma.fcs.sweetshop.domain.invoices.IInvoiceAnnullmentService;
import ua.edu.ukma.fcs.sweetshop.domain.invoices.IInvoiceIssueService;
import ua.edu.ukma.fcs.sweetshop.domain.invoices.Invoice;

public class Order {
    public static enum Status {
        NEW,
        PAID,
        IN_PROCESSING,
        IN_DELIVERY,
        DELIVERED,
        CANCELLED
    }

    private final IInvoiceIssueService invoiceIssueService;
    private final IInvoiceAnnullmentService invoiceAnnullmentService;
    private final IOrderInvoiceFactory orderInvoiceFactory;
    
    private final EntityId id;

    private EntityId customerId;
    private OrderInvoice invoice;

    private final OrderContent content;
    private boolean contentDirty;

    private Status status;
    
    public Order(
        final EntityId id,
        final IInvoiceAnnullmentService invoiceAnnullmentService,
        final IInvoiceIssueService invoiceIssueService,
        final IOrderInvoiceFactory orderInvoiceFactory
    ) {
        this.invoiceAnnullmentService = invoiceAnnullmentService;
        this.invoiceIssueService = invoiceIssueService;
        this.orderInvoiceFactory = orderInvoiceFactory;

        this.id = id;
        this.content = new OrderContent();
        this.contentDirty = false;
        this.status = Status.NEW;
    }
    
    public EntityId getId() {
        return id;
    }

    public Status getStatus() {
        return status;
    }

    // TODO: May we accept OrderItem from outside?? (probably not)
    public void addOrderLine(final OrderItem item, final Integer quantity) {
        this.content.addItem(item, quantity);
        this.contentDirty = true;
    }
    
    // TODO: public void removeOrderLine(...)
    
    public OrderInvoice getOrderInvoice() {
        // If order content has changed since the invoice was issued
        if(this.invoice != null && this.contentDirty) {
            this.invoiceAnnullmentService.annullInvoiceById(invoice.getId());
            this.invoice = null;
        }
        return this.invoice;
    }
    
    public boolean needsPayment() {
        return Status.NEW.equals(this.status);
    }
    
    public OrderInvoice issueInvoice() {
        final Invoice issuedInvoice = this.invoiceIssueService.issueInvoiceForOrder(this);
        this.invoice = this.orderInvoiceFactory.fromInvoice(issuedInvoice);
        return this.invoice;
    }

    public void onOrderInvoicePaid(final EntityId invoiceId) {  // TODO: Do we really need a parameter here?
        if(this.invoice == null) {
            throw new IllegalStateException("This order has no associated invoice");
        }
        if(!this.invoice.getId().equals(invoiceId)) {
            throw new IllegalArgumentException("invoiceId does not match the id of this order's associated invoice");
        }
        if(!this.needsPayment()) {
            throw new IllegalStateException("This order did not require payment");
        }

        this.status = Status.PAID;
    }
}
