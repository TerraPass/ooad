package ua.edu.ukma.fcs.sweetshop.integration.payments.data;

import ua.edu.ukma.fcs.sweetshop.common.data.Card;
import ua.edu.ukma.fcs.sweetshop.domain.invoices.Invoice;

public interface IChargeRequestFactory {
    ChargeRequest createFrom(final Card card, final Invoice invoice);
}
