package ua.edu.ukma.fcs.sweetshop.domain;

import ua.edu.ukma.fcs.sweetshop.domain.EntityId;
import java.util.List;

public interface IRepository<Entity> {
    void insert(final Entity entity);
    void update(final Entity entity);

    void delete(final EntityId id);

    Entity getById(final EntityId id);
    List<Entity> getAll();
}
