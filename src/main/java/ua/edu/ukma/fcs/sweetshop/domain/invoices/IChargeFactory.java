package ua.edu.ukma.fcs.sweetshop.domain.invoices;

import ua.edu.ukma.fcs.sweetshop.domain.invoices.Charge;

public interface IChargeFactory {
    Charge fromChargeOutcome(final ChargeOutcome chargeOutcome);
}
