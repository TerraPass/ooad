package ua.edu.ukma.fcs.sweetshop.web.views;

import javax.ws.rs.core.Response;

public interface IPresenter<Result> {
    Response represent(final Result result);
}
