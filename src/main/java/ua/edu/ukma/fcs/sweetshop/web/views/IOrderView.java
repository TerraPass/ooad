package ua.edu.ukma.fcs.sweetshop.web.views;

import ua.edu.ukma.fcs.sweetshop.application.dto.OrderInvoiceIssueResult;
import ua.edu.ukma.fcs.sweetshop.application.dto.OrderPlacementResult;

public interface IOrderView {
    IPresenter<OrderPlacementResult> getPlacementResultPresenter();
    IPresenter<OrderInvoiceIssueResult> getInvoiceIssueResultPresenter();
    
    IErrorPresenter getErrorPresenter();
}
