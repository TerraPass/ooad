package ua.edu.ukma.fcs.sweetshop.application.dto;

import ua.edu.ukma.fcs.sweetshop.domain.invoices.InvoiceNumber;
import org.apache.commons.lang3.Validate;
import ua.edu.ukma.fcs.sweetshop.application.dto.PaymentResult;
import ua.edu.ukma.fcs.sweetshop.domain.EntityId;

public final class InvoiceCardPaymentResult {
    private final EntityId invoiceId;
    private final InvoiceNumber invoiceNumber;
    private final PaymentResult paymentResult;

    public InvoiceCardPaymentResult(
        final EntityId invoiceId,
        final InvoiceNumber invoiceNumber,
        final PaymentResult paymentResult
    ) {
        Validate.notNull(invoiceId);
        Validate.notNull(invoiceNumber);
        Validate.notNull(paymentResult);

        this.invoiceId = invoiceId;
        this.invoiceNumber = invoiceNumber;
        this.paymentResult = paymentResult;
    }

    public EntityId getInvoiceId() {
        return invoiceId;
    }

    public InvoiceNumber getInvoiceNumber() {
        return invoiceNumber;
    }

    public PaymentResult getPaymentResult() {
        return paymentResult;
    }

    public boolean isSuccessful() {
        return PaymentResult.SUCCESS.equals(this.paymentResult);
    }
}
