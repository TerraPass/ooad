package ua.edu.ukma.fcs.sweetshop.application.dto;

import ua.edu.ukma.fcs.sweetshop.domain.invoices.ChargeOutcome;
import ua.edu.ukma.fcs.sweetshop.domain.invoices.Invoice;

public interface IInvoiceCardPaymentResultFactory {
    InvoiceCardPaymentResult fromAlreadyPaidInvoice(final Invoice invoice);
    InvoiceCardPaymentResult fromAnnulledInvoice(final Invoice invoice);
    InvoiceCardPaymentResult fromChargeOutcome(final Invoice invoice, final ChargeOutcome chargeOutcome);
}
