package ua.edu.ukma.fcs.sweetshop.domain.orders;

import ua.edu.ukma.fcs.sweetshop.domain.EntityId;
import ua.edu.ukma.fcs.sweetshop.domain.invoices.IInvoicePaymentObserver;

public class OrderInvoicePaymentListener implements IInvoicePaymentObserver {
    private IOrderReadRepository orderReadRepository;

    @Override
    public void onInvoicePaid(final EntityId invoiceId) {
        final Order order = orderReadRepository.getByInvoiceId(invoiceId);
        if(order != null) {
            order.onOrderInvoicePaid(invoiceId);
        }
    }
}
