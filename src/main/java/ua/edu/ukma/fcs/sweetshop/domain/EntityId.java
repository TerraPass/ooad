package ua.edu.ukma.fcs.sweetshop.domain;

import java.util.Objects;
import org.apache.commons.lang3.Validate;
import ua.edu.ukma.fcs.sweetshop.domain.utils.IEntityIdGenerator;
import ua.edu.ukma.fcs.sweetshop.domain.utils.SimpleGuidEntityIdGenerator;

public final class EntityId {
    private static final IEntityIdGenerator DEFAULT_GENERATOR = new SimpleGuidEntityIdGenerator();

    private final String value;

    public EntityId(final String value) {
        Validate.notBlank(value, "EntityId value must not be null or empty");

        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static EntityId generate(final IEntityIdGenerator generator) {
        return generator.generate();
    }

    public static EntityId generate() {
        return generate(DEFAULT_GENERATOR);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.value);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EntityId other = (EntityId) obj;
        if (!Objects.equals(this.value, other.value)) {
            return false;
        }
        return true;
    }
}
