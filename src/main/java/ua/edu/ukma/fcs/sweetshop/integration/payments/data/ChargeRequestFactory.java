package ua.edu.ukma.fcs.sweetshop.integration.payments.data;

import org.apache.commons.lang3.Validate;
import ua.edu.ukma.fcs.sweetshop.common.data.Card;
import ua.edu.ukma.fcs.sweetshop.domain.invoices.Invoice;

public class ChargeRequestFactory implements IChargeRequestFactory {
    private final IChargeDescriber chargeDescriber;
    
    public ChargeRequestFactory(final IChargeDescriber chargeDescriber) {
        Validate.notNull(chargeDescriber, "chargeDescriber must not be null");

        this.chargeDescriber = chargeDescriber;
    }

    @Override
    public ChargeRequest createFrom(final Card card, final Invoice invoice) {
        return new ChargeRequest(
            card,
            invoice.getMoneySum(),
            this.chargeDescriber.describeChargeFor(invoice)
        );
    }
}
