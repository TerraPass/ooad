package ua.edu.ukma.fcs.sweetshop.integration.payments.data;

import ua.edu.ukma.fcs.sweetshop.domain.invoices.Invoice;

public interface IChargeDescriber {
    String describeChargeFor(final Invoice invoice);
}
