package ua.edu.ukma.fcs.sweetshop.web.views;

import ua.edu.ukma.fcs.sweetshop.application.dto.InvoiceCardPaymentResult;

public interface IInvoiceView {
    IPresenter<InvoiceCardPaymentResult> getCardPaymentResultPresenter();

    IErrorPresenter getErrorPresenter();
}
