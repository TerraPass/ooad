package ua.edu.ukma.fcs.sweetshop.application.dto;

import ua.edu.ukma.fcs.sweetshop.domain.orders.OrderInvoice;

public interface IOrderInvoiceIssueResultFactory {
    OrderInvoiceIssueResult fromNoSuchOrder();
    OrderInvoiceIssueResult fromNewOrderInvoice(final OrderInvoice orderInvoice);
}
