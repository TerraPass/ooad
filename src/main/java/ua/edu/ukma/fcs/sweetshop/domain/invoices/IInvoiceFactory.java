package ua.edu.ukma.fcs.sweetshop.domain.invoices;

import ua.edu.ukma.fcs.sweetshop.domain.orders.Order;

public interface IInvoiceFactory {
    Invoice fromOrder(final Order order);
}
