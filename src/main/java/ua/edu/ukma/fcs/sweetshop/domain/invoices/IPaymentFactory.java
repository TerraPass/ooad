package ua.edu.ukma.fcs.sweetshop.domain.invoices;

import ua.edu.ukma.fcs.sweetshop.domain.invoices.Payment;

public interface IPaymentFactory {
    Payment createCardPayment();
}
