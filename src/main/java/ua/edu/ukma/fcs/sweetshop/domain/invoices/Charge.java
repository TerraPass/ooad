package ua.edu.ukma.fcs.sweetshop.domain.invoices;

import ua.edu.ukma.fcs.sweetshop.common.data.MoneySum;
import org.apache.commons.lang3.Validate;
import ua.edu.ukma.fcs.sweetshop.domain.EntityId;

public final class Charge {
    private final EntityId id;

    private final String stripeId;
    private final String description;

    public Charge(
        final EntityId id,
        final EntityId paymentId,
        final String stripeId,
        final MoneySum moneySum,
        final String description
    ) {
        Validate.notNull(id, "id must not be null");
        Validate.notBlank(stripeId, "stripeId must not be null or empty string");
        Validate.notNull(moneySum, "moneySum must not be null");
        Validate.notNull(description, "description must not be null");
        
        this.id = id;
        this.stripeId = stripeId;
        this.description = description;
    }

    public EntityId getId() {
        return id;
    }
    
    public String getStripeId() {
        return stripeId;
    }

    public String getDescription() {
        return description;
    }
}
