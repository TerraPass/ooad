package ua.edu.ukma.fcs.sweetshop.application;

import ua.edu.ukma.fcs.sweetshop.application.dto.OrderFormData;
import ua.edu.ukma.fcs.sweetshop.application.dto.OrderPlacementResult;

public interface IOrderPlacementService {
    OrderPlacementResult placeOrder(final OrderFormData orderFormData);
}
