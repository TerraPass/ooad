package ua.edu.ukma.fcs.sweetshop.domain.invoices;

import ua.edu.ukma.fcs.sweetshop.domain.invoices.Invoice;
import ua.edu.ukma.fcs.sweetshop.common.data.Card;

public interface IChargeService {
    // TODO: Combine Invoice, card and billing address into a single DTO?
    ChargeOutcome chargeByInvoice(final Invoice invoice, final Card card);
}
