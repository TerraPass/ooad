package ua.edu.ukma.fcs.sweetshop.domain.invoices;

import ua.edu.ukma.fcs.sweetshop.domain.invoices.Invoice;
import org.apache.commons.lang3.Validate;
import ua.edu.ukma.fcs.sweetshop.common.data.Card;
import ua.edu.ukma.fcs.sweetshop.integration.payments.ICardCharger;
import ua.edu.ukma.fcs.sweetshop.integration.payments.data.ChargeRequest;
import ua.edu.ukma.fcs.sweetshop.integration.payments.data.ChargeResult;
import ua.edu.ukma.fcs.sweetshop.integration.payments.data.IChargeRequestFactory;

public class ChargeService implements IChargeService {
    private final ICardCharger cardCharger;
    private final IChargeRequestFactory chargeRequestFactory;
    private final IChargeOutcomeFactory chargeOutcomeFactory;

    public ChargeService(
        final ICardCharger cardCharger,
        final IChargeRequestFactory chargeRequestFactory,
        final IChargeOutcomeFactory chargeOutcomeFactory
    ) {
        Validate.notNull(cardCharger, "cardCharger must not be null");
        Validate.notNull(chargeRequestFactory, "chargeRequestFactory must not be null");
        Validate.notNull(chargeOutcomeFactory, "chargeOutcomeFactory must not be null");

        this.cardCharger = cardCharger;
        this.chargeRequestFactory = chargeRequestFactory;
        this.chargeOutcomeFactory = chargeOutcomeFactory;
    }
    
    @Override
    public ChargeOutcome chargeByInvoice(final Invoice invoice, final Card card) {
        final ChargeRequest chargeRequest = chargeRequestFactory.createFrom(card, invoice);

        final ChargeResult chargeResult = cardCharger.chargeCard(chargeRequest);

        return chargeOutcomeFactory.fromChargeResult(chargeResult);
    }
}
