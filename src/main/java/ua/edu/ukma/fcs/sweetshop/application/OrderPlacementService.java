package ua.edu.ukma.fcs.sweetshop.application;

import ua.edu.ukma.fcs.sweetshop.domain.IRepository;
import ua.edu.ukma.fcs.sweetshop.domain.orders.IOrderFactory;
import ua.edu.ukma.fcs.sweetshop.domain.orders.Order;
import org.apache.commons.lang3.Validate;
import ua.edu.ukma.fcs.sweetshop.application.dto.IOrderPlacementResultFactory;
import ua.edu.ukma.fcs.sweetshop.application.dto.OrderFormData;
import ua.edu.ukma.fcs.sweetshop.application.dto.OrderPlacementResult;

public class OrderPlacementService implements IOrderPlacementService {
    private final IOrderFactory orderFactory;
    private final IRepository<Order> orderRepository;

    private final IOrderPlacementResultFactory orderPlacementResultFactory;

    public OrderPlacementService(
        final IOrderFactory orderFactory,
        final IRepository<Order> orderRepository,
        final IOrderPlacementResultFactory orderPlacementResultFactory
    ) {
        Validate.notNull(orderFactory, "orderFactory must not be null");
        Validate.notNull(orderRepository, "orderRepository must not be null");
        Validate.notNull(orderPlacementResultFactory, "orderPlacementResultFactory must not be null");

        this.orderFactory = orderFactory;
        this.orderRepository = orderRepository;
        this.orderPlacementResultFactory = orderPlacementResultFactory;
    }
    
    @Override
    public OrderPlacementResult placeOrder(final OrderFormData orderFormData) {
        final Order order = this.orderFactory.fromFormData(orderFormData);
        this.orderRepository.insert(order);

        return this.orderPlacementResultFactory.fromNewOrder(order);
    }
}
