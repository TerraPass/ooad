package ua.edu.ukma.fcs.sweetshop.domain.orders;

import ua.edu.ukma.fcs.sweetshop.domain.EntityId;
import ua.edu.ukma.fcs.sweetshop.domain.invoices.InvoiceNumber;

public class OrderInvoice {
    private final EntityId id;
    private final InvoiceNumber invoiceNumber;
    private final boolean paid;

    public OrderInvoice(final EntityId id, final InvoiceNumber invoiceNumber, final boolean paid) {
        this.id = id;
        this.invoiceNumber = invoiceNumber;
        this.paid = paid;
    }

    public EntityId getId() {
        return id;
    }

    public InvoiceNumber getInvoiceNumber() {
        return invoiceNumber;
    }

    public boolean isPaid() {
        return paid;
    }
}
