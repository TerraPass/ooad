package ua.edu.ukma.fcs.sweetshop.domain.invoices;

import org.apache.commons.lang3.Validate;

public final class ChargeOutcome {
    public enum ChargeErrorKind {
        NONE,
        REQUEST_ERROR,
        TRY_AGAIN
    }

    private final ChargeErrorKind errorKind;
    private final String stripeChargeId;
    private final String chargeDescription;

    public ChargeOutcome(final ChargeErrorKind errorKind, final String stripeChargeId, final String chargeDescription) {
        Validate.notNull(errorKind);
        Validate.isTrue(
            stripeChargeId != null || errorKind != ChargeErrorKind.NONE,
            "if errorKind is NONE, stripeChargeId must be present"
        );
        Validate.notBlank(chargeDescription, "chargeDescription must not be blank");

        this.errorKind = errorKind;
        this.stripeChargeId = stripeChargeId;
        this.chargeDescription = chargeDescription;
    }
    
    public ChargeOutcome(final String stripeId, final String chargeDescription) {
        this(ChargeErrorKind.NONE, stripeId, chargeDescription);
    }

    public ChargeErrorKind getErrorKind() {
        return errorKind;
    }

    public String getStripeChargeId() {
        if(!this.isSuccessful()) {
            throw new IllegalStateException("stripeChargeId can only be retrieved from a successful ChargeOutcome");
        }

        return stripeChargeId;
    }

    public String getChargeDescription() {
        return chargeDescription;
    }

    public boolean isSuccessful() {
        return ChargeErrorKind.NONE.equals(this.errorKind);
    }
}
