package ua.edu.ukma.fcs.sweetshop.web.controllers;

import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.lang3.Validate;
import ua.edu.ukma.fcs.sweetshop.application.IOrderInvoiceIssueService;
import ua.edu.ukma.fcs.sweetshop.application.IOrderPlacementService;
import ua.edu.ukma.fcs.sweetshop.application.dto.IOrderFormDataFactory;
import ua.edu.ukma.fcs.sweetshop.application.dto.IOrderInvoiceRequestFactory;
import ua.edu.ukma.fcs.sweetshop.application.dto.InvalidFormDataException;
import ua.edu.ukma.fcs.sweetshop.application.dto.OrderFormData;
import ua.edu.ukma.fcs.sweetshop.application.dto.OrderInvoiceIssueResult;
import ua.edu.ukma.fcs.sweetshop.application.dto.OrderInvoiceRequest;
import ua.edu.ukma.fcs.sweetshop.application.dto.OrderPlacementResult;
import ua.edu.ukma.fcs.sweetshop.web.forms.OrderInvoiceRequestRawFormData;
import ua.edu.ukma.fcs.sweetshop.web.forms.OrderPlacementRawFormData;
import ua.edu.ukma.fcs.sweetshop.web.views.IOrderView;

/**
 * This controller is responsible for processing user actions,
 * related to placing and processing orders.
 */
@Path("orders")
public final class OrdersController {
    private final IOrderView orderView;
    
    private final IOrderFormDataFactory orderFormDataFactory;
    private final IOrderInvoiceRequestFactory orderInvoiceRequestFactory;

    private final IOrderPlacementService orderPlacementService;
    private final IOrderInvoiceIssueService orderInvoiceIssueService;

    public OrdersController(
        final IOrderView orderView,
        final IOrderFormDataFactory orderFormDataFactory,
        final IOrderInvoiceRequestFactory orderInvoiceRequestFactory,
        final IOrderPlacementService orderPlacementService,
        final IOrderInvoiceIssueService orderInvoiceIssueService
    ) {
        Validate.notNull(orderView);
        Validate.notNull(orderFormDataFactory);
        Validate.notNull(orderInvoiceRequestFactory);
        Validate.notNull(orderPlacementService);
        Validate.notNull(orderInvoiceIssueService);

        this.orderView = orderView;
        this.orderFormDataFactory = orderFormDataFactory;
        this.orderInvoiceRequestFactory = orderInvoiceRequestFactory;
        this.orderPlacementService = orderPlacementService;
        this.orderInvoiceIssueService = orderInvoiceIssueService;
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response placeOrder(final OrderPlacementRawFormData rawFormData) {
        try {
            final OrderFormData formData = this.orderFormDataFactory.fromRawFormData(rawFormData);

            final OrderPlacementResult result = this.orderPlacementService.placeOrder(formData);

            return this.orderView.getPlacementResultPresenter().represent(result);
        } catch (InvalidFormDataException e) {
            return this.orderView.getErrorPresenter().representValidationFailure(e);
        }
    }

    @PUT
    @Path("{id}/invoice.json")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response issueInvoice(
        @PathParam("id") final String orderId,
        final OrderInvoiceRequestRawFormData rawFormData
    ) {
        rawFormData.setOrderId(orderId);

        try {
            final OrderInvoiceRequest orderInvoiceRequest 
                = this.orderInvoiceRequestFactory.fromRawFormData(rawFormData);

            final OrderInvoiceIssueResult result
                = this.orderInvoiceIssueService.issueOrderInvoice(orderInvoiceRequest);
            
            return this.orderView.getInvoiceIssueResultPresenter().represent(result);
        } catch (InvalidFormDataException e) {
            return this.orderView.getErrorPresenter().representValidationFailure(e);
        }
    }
}
